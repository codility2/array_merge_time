import pytest
from main import mergeSums, accSums

def test_one_num_returns_0():
    assert 0 == mergeSums([100])

def test_two_nums_to_sum():
    assert 300 == mergeSums([100, 200])

def test_thre_nums_to_min_sum():
    assert 1700 == mergeSums([1000,100,250])

def test_min_sum_calculated():
    assert 14 == mergeSums([4,2,3])
    assert 28 == mergeSums([5,4,2,3])

