from typing import List

def accSums(nums: List[int]) -> List[int]:
   if len(nums) < 2:
       return [0]
   snums = sorted(nums)
   first, second, *rest = snums
   sums = [first + second]
   for num in rest:
       sums.append(sums[-1] + num)
   return sums

def mergeSums(nums: List[int]) -> int:
    return sum(accSums(nums))

        


